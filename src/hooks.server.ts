import { sequence } from '@sveltejs/kit/hooks';
import prisma from '$lib/hook/prisma';
import lucia from '$lib/hook/lucia';

export const handle = sequence(prisma, lucia);
