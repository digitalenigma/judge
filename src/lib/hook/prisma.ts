import type { Handle } from '@sveltejs/kit';
import { PrismaClient } from '@prisma/client';

const handle = (async ({ event, resolve }) => {
	const prisma = new PrismaClient();
	event.locals.prisma = prisma;
	const response = await resolve(event);
	return response;
}) satisfies Handle;

export default handle;
