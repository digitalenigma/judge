import type { Handle } from '@sveltejs/kit';
import { initLucia } from '$lib/server/lucia';

const handle = (async ({ event, resolve }) => {
	const { prisma } = event.locals;
	const lucia = initLucia(prisma);
	event.locals.lucia = lucia;
	event.locals.user = null;
	event.locals.session = null;
	
	const sessionId = event.cookies.get(lucia.sessionCookieName);
	if (!sessionId) {
		event.locals.user = null;
		event.locals.session = null;
		return await resolve(event);
	}

	const { session, user: userId } = await lucia.validateSession(sessionId);
	if (session && session.fresh) {
		const sessionCookie = lucia.createSessionCookie(session.id);
		// sveltekit types deviates from the de-facto standard
		// you can use 'as any' too
		event.cookies.set(sessionCookie.name, sessionCookie.value, {
			path: '.',
			...sessionCookie.attributes
		});
	}


	if (!session) {
		const sessionCookie = lucia.createBlankSessionCookie();
		event.cookies.set(sessionCookie.name, sessionCookie.value, {
			path: '.',
			...sessionCookie.attributes
		});
	}

	if (userId?.id) {
		const user = await event.locals.prisma.user.findFirst({
			select: { id: true, username: true },
			where: { id: userId.id },
		});
		event.locals.user = user;
		event.locals.session = session;
	}
	return resolve(event);
}) satisfies Handle;

export default handle;
