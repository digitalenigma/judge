import { PrismaAdapter } from '@lucia-auth/adapter-prisma';
import { Lucia } from 'lucia';
import { dev } from '$app/environment';
import type { PrismaClient } from '@prisma/client';

export const initLucia = (prisma: PrismaClient) => {
	const adapter = new PrismaAdapter(prisma.session, prisma.user);
	return new Lucia(adapter, {
		sessionCookie: {
			attributes: {
				// set to `true` when using HTTPS
				secure: !dev
			}
		}
	});
};
