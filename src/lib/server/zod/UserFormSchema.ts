import { UserSchema } from "$lib/zod";
import { z } from "zod";

export const UserFormSchema = UserSchema.extend({
    password_repeat: z.string()
});