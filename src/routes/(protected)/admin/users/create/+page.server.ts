import { zod } from 'sveltekit-superforms/adapters';
import type { PageServerLoad, Actions } from './$types';
import { fail, message, setError, superValidate } from 'sveltekit-superforms';
import { UserFormSchema } from '$lib/server/zod/UserFormSchema';
import { Argon2id } from 'oslo/password';

export const load = (async () => {
	const form = await superValidate(zod(UserFormSchema));
	return { form };
}) satisfies PageServerLoad;

export const actions = {
	default: async ({ request, locals }) => {
        const form = await superValidate(request, zod(UserFormSchema));
        if (form.data.hashed_password.length < 8) {
            setError(form, 'hashed_password', 'Password should be at least 8 characters.');
        } else if (form.data.hashed_password !== form.data.password_repeat) {
            setError(form, 'password_repeat', 'Password did not match.');
        }
        if (!form.valid) {
            return fail(400, { form });
        }
        await locals.prisma.user.create({
            data: {
                username: form.data.username,
                hashed_password: await new Argon2id().hash(form.data.hashed_password),
            }
        });
        return message(form, 'User saved successfully.');
	}
} satisfies Actions;
