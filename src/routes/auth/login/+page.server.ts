import { message, setError, superValidate, type SuperValidated } from 'sveltekit-superforms';
import type { PageServerLoad, Actions } from './$types';
import { zod } from 'sveltekit-superforms/adapters';
import { UserSchema } from '$lib/zod';
import { Argon2id } from 'oslo/password';
import type { z } from 'zod';
import type { Prisma, PrismaClient } from '@prisma/client';
import { redirect, type Cookies } from '@sveltejs/kit';

export const load = (async () => {
	const form = await superValidate(zod(UserSchema));
	return { form };
}) satisfies PageServerLoad;

export const actions = {
	default: async ({ request, locals, cookies }) => {
		const form = await superValidate(request, zod(UserSchema));
		const user = await isLoginValid(locals, form);
		if (!user) return message(form, 'Invalid username and password', { status: 400 });
		await createSession(locals, user, cookies);
		throw redirect(303, '/');
	}
} satisfies Actions;

const createSession = async (locals: App.Locals, user: Prisma.UserGetPayload<null>, cookies: Cookies) => {
	const { lucia } = locals;
	const session = await lucia.createSession(user.id, {});
	const sessionCookie = lucia.createSessionCookie(session.id);
	cookies.set(sessionCookie.name, sessionCookie.value, {
		path: ".",
		...sessionCookie.attributes
	});
};

const isLoginValid = async (locals: App.Locals, form: SuperValidated<z.infer<typeof UserSchema>>) => {
	if (!form.valid) {
		return false
	}
	const user = await locals.prisma.user.findFirst({
		where: { username: form.data.username }
	});
	if (!user) return false;

	const isValidPassword = new Argon2id().verify(user?.hashed_password, form.data.hashed_password);
	if (!isValidPassword) return false;
	return user;
};