import { redirect } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';

export const load = (async ({ locals }) => {
    if (locals.session?.id) await locals.lucia.invalidateSession(locals.session.id);
    throw redirect(303, '/');
}) satisfies PageServerLoad;