// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
import type { PrismaClient } from '@prisma/client';
import type { Lucia } from 'lucia';
import type { User, Session } from 'lucia';

declare global {
	namespace App {
		// interface Error {}
		interface Locals {
			prisma: PrismaClient;
			lucia: Lucia;
			user: User | null;
			session: Session | null;
		}
		// interface PageData {}
		// interface PageState {}
		// interface Platform {}
	}
}

export {};
